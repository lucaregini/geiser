Installing Geiser.
------------------

Geiser is usable from its source tree, with no configuration
whatsoever, or can be installed from ELPA with `M-x install-package'
is Marmalade is in your list of archives.  You can also (byte) compile
and install it with the usual configure/make/make install dance.

* From ELPA

Add Marmalade to your `package-archives' list:

  (require 'package)
  (add-to-list 'package-archives
    '("marmalade" . "http://marmalade-repo.org/packages/"))
  (package-initialize)

and run `M-x install-package RET geiser`.  You can also use
http://download.savannah.gnu.org/releases/geiser/packages as a repo,
or download directly the package from there and use M-x
package-install-file.

* In place
    - Extract the tarball or clone the git repository anywhere in your
      file system. Let's call that place <path-to-geiser>.
    - In your .emacs:

      (load-file "<path-to-geiser>/elisp/geiser.el")

* Byte-compiled
    - Create a build directory, `build', say:
      $ cd <path-to-geiser>
      $ mkdir build; cd build
    - Configure and make:
      $ ../configure && make

    Now, you can use the byte-compiled Geiser in place by adding to
    your .emacs:

    (load "<path-to-geiser>/build/elisp/geiser-load")

    or, alternatively, install it with:

    $ make install

    (you might need to get root access, depending on your installation
    directory) and, instead of the above load forms, require
    'geiser-install (not 'geiser, mind you) in your emacs
    initialization file:

    (require 'geiser-install)

* Chicken Addendum
    These steps are necessary to fully support Chicken Scheme, but are
    not required for any other scheme.

    - Install the necessary support eggs:
      $ chicken-install -s apropos chicken-doc
    - Update the Chicken documentation database:
      $ cd `csi -p '(chicken-home)'`
      $ curl http://3e8.org/pub/chicken-doc/chicken-doc-repo.tgz | sudo tar zx

    You're ready to go!

* Gambit Addendum
  These steps are necessary to fully support all Gambit Scheme functionalities,
  but are not required for any other scheme.

  # SETUP :
    1 - clone the last version of gambit and of this repo
    2 - configure gambit using --enable-rtlib-debug-source to activate autodoc
    ``` bash
    $ cd ~/
    $ git clone "the gambit repo address"
    $ cd gambit
    $ ./configure --enable-single-host --enable-debug --enable-rtlib-debug-source
    $ make bootstrap
    $ make bootclean
    $ make -j
    $ make install
    ```
    3 - put a link to geiser/elisp/geiser.el in your init file
    in your init file paste :
    ``` elisp
    (load-file "~/geiser/elisp/geiser.el")
    ```
    (the path is your path to geiser.el.)

  # setup REMOTE CONNECT TO A GAMBIT REPL
    1 - you need to enable the gambit/geiser module
    ( if you can't,  substitute the command below
      with the PS: at the end of the readme )
    ``` bash
    $ mkdir ~/gambit/lib/gambit/geiser
    $ cp ~/geiser/geiser-module/* ~/gambit/lib/geiser/
    ```
    2 - now that you have the module you start gsi with it and using the -:d@
    ``` bash
    $ gsi -:d gambit/geiser -
    ```
    3 - you can now open emacs and write
    ```
    M-x geiser-connect gambit
    ```
  Enjoy !

By the way, if you are unable to use gambit modules,
open gsi with the gambit.scm file
located in geiser/scheme/gambit/geiser/gambit.scm

something like : gsi -:d@ ~/geiser/scheme/gambit/geiser/gambit.scm -

Geiser's makefile accepts also all those other standard autotools
targets that you've come to know and love and that are documented
in virtually all boilerplate INSTALL files out there.
